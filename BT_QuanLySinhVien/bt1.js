function xepLoai() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var loaiSV = document.getElementById("loaiSV").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemVan = document.getElementById("txtDiemVan").value * 1;

  console.log({ maSv, tenSv, loaiSV, diemToan, diemVan });

  var SV = {
    ten: tenSv,
    ma: maSv,
    loai: loaiSV,
    diemToan: diemToan,
    diemVan: diemVan,
    // taọ hàm để tính điểm trung bình
    tinhDiemTrungBinh: function () {
      return (this.diemToan * 1 + this.diemVan * 1) / 2;
    },
    // tạo hàm để tính xếp loại
    xepLoai: function () {
      if (this.tinhDiemTrungBinh() >= 8) {
        return "Giỏi";
      } else if (this.tinhDiemTrungBinh() >= 6.5) {
        return "Khá";
      } else {
        return "Trung Bình";
      }
    },
  };

  //xuất kết quả:
  document.getElementById("spanTenSV").innerHTML = SV.ten;
  document.getElementById("spanMaSV").innerHTML = SV.ma;
  document.getElementById("spanLoaiSV").innerHTML = SV.loai;
  document.getElementById("spanDTB").innerHTML = SV.tinhDiemTrungBinh();
  document.getElementById("spanXepLoai").innerHTML = SV.xepLoai();
}
