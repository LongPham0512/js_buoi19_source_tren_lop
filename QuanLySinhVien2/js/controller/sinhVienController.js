function layThongTinTuForm() {
  var maSV = document.getElementById("txtMaSV").value;
  var tenSV = document.getElementById("txtTenSV").value;
  var emailSV = document.getElementById("txtEmail").value;
  var Toan = document.getElementById("txtDiemToan").value * 1;
  var Ly = document.getElementById("txtDiemLy").value * 1;
  var Hoa = document.getElementById("txtDiemHoa").value * 1;
  var sinhVien = new SinhVien(maSV, tenSV, emailSV, Toan, Ly, Hoa);
  console.log(sinhVien);

  return sinhVien;
}

// Xuất danh sách sinh viên
function xuatDanhSachSinhVien(dssv) {
  console.log({ dssv });
  var contentHTML = "";
  for (var index = 0; index < dssv.length; index++) {
    var sinhVien = dssv[index];
    console.log(sinhVien.maSV);
    var contentTrTag = /*html*/ `<tr> 
    <td>${sinhVien.maSV}</td>
    <td>${sinhVien.tenSV}</td>
    <td>${sinhVien.email}</td>
    <td>${sinhVien.tinhDiemTrungBinh()}</td>
    <td>
    <button class="btn btn-success" onclick="suaSinhVien('${
      sinhVien.maSV
    }')">Sửa</button>
    <button class="btn btn-danger" onclick="xoaSinhVien('${
      sinhVien.maSV
    }')">Xóa</button>
    </td>
    </tr>`;

    contentHTML += contentTrTag;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// xuất thông tin lên form
function xuatThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.maSV;
  document.getElementById("txtTenSV").value = sv.tenSV;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
