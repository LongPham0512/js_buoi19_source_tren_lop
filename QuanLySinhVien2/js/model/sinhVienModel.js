//tạo đối tượng sinh viên
var SinhVien = function (_ma, _ten, _email, _toan, _ly, _hoa) {
  this.maSV = _ma;
  this.tenSV = _ten;
  this.email = _email;
  this.diemToan = _toan;
  this.diemLy = _ly;
  this.diemHoa = _hoa;

  this.tinhDiemTrungBinh = function () {
    return (this.diemToan + this.diemLy + this.diemHoa) / 3;
  };
};
