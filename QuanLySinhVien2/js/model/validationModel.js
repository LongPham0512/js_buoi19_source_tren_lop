function ValidatorSV() {
  this.kiemTraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim(); // hàm trim(): loại bỏ khoảng cách 2 đầu khi user nhấn space

    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  this.kiemTraIdHopLe = function (newSinhVien, danhSachSinhVien) {
    console.log({ newSinhVien, danhSachSinhVien });
    var index = danhSachSinhVien.findIndex(function (item) {
      return item.maSV == newSinhVien.maSV;
    });
    console.log(index);

    if (index == -1) {
      document.getElementById("spanMaSV").innerText = "";

      console.log("yes íid");
      return true;
    }

    document.getElementById("spanMaSV").innerText =
      "Mã Sinh Viên khum được trùng";
    return false;
  };

  this.kiemTraEmail = function (idTarget, idError) {
    let parten = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    console.log(parten.test(valueInput), valueInput);
    document.getElementById(idError).innerText = "Email khum hợp lệ";
    return false;
  };
}
