var danhSachSinhVien = [];

var validatorSV = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSV == id;
  });
};

const luuLocalStorage = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

//lấy dữ liệu từ localstorage khi user tải lại trang
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);

// gán ngược lại cho array gốc và render lại giao diện
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);
  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSV,
      item.tenSV,
      item.email,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();

  var isValid = true;

  var index = danhSachSinhVien.findIndex(function (item) {
    return item.maSV == newSinhVien.maSV;
  });

  var isValidMaSV =
    validatorSV.kiemTraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã sinh viên khum được rỗng"
    ) && validatorSV.kiemTraIdHopLe(newSinhVien, danhSachSinhVien);

  var isValidEmail =
    validatorSV.kiemTraRong(
      "txtEmail",
      "spanEmailSV",
      "Mã sinh viên khum được rỗng"
    ) && validatorSV.kiemTraEmail("txtEmail", "spanEmailSV");

  isValid = isValidMaSV && isValidEmail;

  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);
    //* convert array thành JSON để có thể lưu vào localStorage
    luuLocalStorage();
  }
}

function xoaSinhVien(id) {
  console.log(id);

  var viTri = timKiemViTri(id, danhSachSinhVien);
  console.log({ viTri });

  //xoá tại vị trí tìm thấy vs số lượng là 1
  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSinhVien(danhSachSinhVien);

  // convert array thành JSON để có thể lưu vào localStorage
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);
  console.log({ viTri });

  var sinhVien = danhSachSinhVien[viTri];

  xuatThongTinLenForm(sinhVien);
}

function capNhatSinhVien() {
  var sinhVienEdit = layThongTinTuForm();
  console.log({ sinhVienEdit });

  let viTri = timKiemViTri(sinhVienEdit.maSV, danhSachSinhVien);
  danhSachSinhVien[viTri] = sinhVienEdit;
  xuatDanhSachSinhVien(danhSachSinhVien);

  // convert array thành JSON để có thể lưu vào localStorage
  luuLocalStorage();
}

function timSinhVien() {
  var danhSachSinhVienCanTim = [];
  var tenSinhVienCanTim = document.getElementById("txtSearch").value;

  danhSachSinhVien.forEach(function (item) {
    if (
      item.tenSV == tenSinhVienCanTim ||
      item.maSV == tenSinhVienCanTim ||
      item.email == tenSinhVienCanTim
    ) {
      danhSachSinhVienCanTim.push(item);
    }

    xuatDanhSachSinhVien(danhSachSinhVienCanTim);
  });
}

function resetDanhSachSinhVien() {
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function hienLaiDSSVKhiXoaTextSearch() {
  var txtSearch = document.getElementById("txtSearch").value;
  if (txtSearch == "") {
    xuatDanhSachSinhVien(danhSachSinhVien);
  }
}
