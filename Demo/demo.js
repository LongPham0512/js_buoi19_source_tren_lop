// objecr là pass by reference
// array [] => lưu trữ danh sách các phần tử (đối tượng) cùng lọai

// object {} => mô tả thông tin chi tiết về 1 đối tượng

function dog(_username, _age, _gender) {
  this.username = _username;
  this.age = _age;
  this.gender = _gender;

  this.speak = function () {
    console.log("Gâu Gâu");
  };
}

var dogGreen = new dog("Mực", 2, "Male");
console.log("dogGreen", dogGreen);
dogGreen.speak();
